

from main.models import IndexDesc, Person


class BMIcontroller:

    #responses
    @staticmethod
    def bmi_response_for_get(request):
        from main.serializers import PersonSerializer
        data = BMIcontroller.extract_data_from_request(request)
        validator = PersonSerializer(data=data)
        ret = BMIcontroller.validate(validator)
        index_ = None
        desc = None
        if ret:
            index_ = BMIcontroller.calculateBMI(validator.validated_data['mass'], validator.validated_data['height'])
            desc = BMIcontroller.get_desc_for_index(index_)
        return {'success': ret, 'index': index_, 'desc': desc, 'errors': validator.errors}

    @staticmethod
    def bmi_response_for_post(request):
        from main.serializers import AddPersonSerializer
        data = BMIcontroller.extract_data_from_request(request)
        print(data)
        validator = AddPersonSerializer(data=data)
        ret = BMIcontroller.validate(validator)
        if ret:
            validator.save()
        return {'success': ret, 'errors': validator.errors}

    #tools
    @staticmethod
    def get_desk_model_by_index(index_: float):
        qs = IndexDesc.objects.filter(min__lte=index_, max__gte=index_)
        return qs.first()

    @staticmethod
    def get_desc_for_index(index_: float):
        obj = BMIcontroller.get_desk_model_by_index(index_)
        if obj:
            return obj.desc

    @staticmethod
    def calculateBMI(mass, height):
        return mass / (height / 100)**2

    @staticmethod
    def extract_data_from_request(request):
        if request.method == 'GET':
            extract_obj = request.query_params
        elif request.method == 'POST':
            extract_obj = request.data
        else:
            return
        mass = extract_obj.get('mass')
        height = extract_obj.get('height')
        age = extract_obj.get('age')
        fio = extract_obj.get('fio')
        return {'mass': mass, 'height': height, 'age': age, 'fio': fio}


    @staticmethod
    def validate(validator):
        return validator.is_valid()



def get_statistic_by_age_range(min_, max_):
    filter_kwargs = {
        'age__lte': max_,
        'age__gte': min_,
    }

    out = []
    index_qs = IndexDesc.objects.all()
    persons_count = len(Person.objects.all().filter(**filter_kwargs))
    for index_obj in index_qs:
        persons_count_ = len(index_obj.person_set.all().filter(**filter_kwargs))
        if persons_count_ > 0:
            percent = persons_count_ / persons_count * 100
        else:
            percent = 0
        out.append({
            'desc': index_obj.desc,
            'percent': percent,
        })
    return out

