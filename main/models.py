from django.db import models


class Person(models.Model):
    fio = models.CharField(max_length=100)
    mass = models.PositiveSmallIntegerField()
    height = models.PositiveSmallIntegerField()
    age = models.PositiveSmallIntegerField()
    index = models.FloatField()
    index_category = models.ForeignKey('IndexDesc', null=True, on_delete=models.DO_NOTHING)


class IndexDesc(models.Model):
    min = models.PositiveSmallIntegerField()
    max = models.PositiveSmallIntegerField()
    desc = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.min} {self.max}'