from django.urls import path
from main.views import *

urlpatterns = [
    path('ab/', AB_BMI.as_view(), name='ab'),
    path('c/', C_Statistic.as_view(), name='c'),
    path('d/', D_Procedure.as_view(), name='d'),
]