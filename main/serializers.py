from rest_framework import serializers
from main.logic import BMIcontroller
from main.models import Person


class PersonSerializer(serializers.ModelSerializer):
    mass = serializers.IntegerField(max_value=200, min_value=1)
    height = serializers.IntegerField(max_value=500, min_value=50)

    class Meta:
        model = Person
        fields = ('mass', 'height')


class AddPersonSerializer(serializers.ModelSerializer):
    mass = serializers.IntegerField(max_value=200, min_value=1)
    height = serializers.IntegerField(max_value=500, min_value=50)
    fio = serializers.CharField(max_length=100)
    age = serializers.IntegerField(max_value=150, min_value=0)

    class Meta:
        model = Person
        fields = ('mass', 'height', 'age', 'fio')

    def create(self, validated_data):
        index_ = BMIcontroller.calculateBMI(validated_data['mass'], validated_data['height'])
        desc_obj = BMIcontroller.get_desk_model_by_index(index_)
        validated_data.update({
            'index': index_,
            'index_category': desc_obj,
        })
        return Person.objects.create(**validated_data)