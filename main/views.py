from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from main.serializers import *
from main import logic
from main.models import IndexDesc


class AB_BMI(APIView):

    def get(self, request):
        resp = logic.BMIcontroller.bmi_response_for_get(request)
        return Response(resp)

    def post(self, request):
        resp = logic.BMIcontroller.bmi_response_for_post(request)
        return Response(resp)


class C_Statistic(APIView):

    def get(self, request):
        out = logic.get_statistic_by_age_range(0, 200)
        return Response(out)


class D_Procedure(APIView):

    def get(self, request):
        out = []
        age_list = [i*10 for i in range(15)]
        for index, value in enumerate(age_list):
            if index == 0:
                continue
            prev = age_list[index-1] + 1
            out_ = logic.get_statistic_by_age_range(prev, value)
            out.append({
                'min': prev,
                'max': value,
                'data': out_,
            })

        return Response(out)